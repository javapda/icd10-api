# icd10-api #

* permission-based api to access ICD10 data

# install / setup

## install ##
```
 npm install icd10-api --save
```
## usage ##

* node
```
const {Icd10Api} = require('icd10-api')

new Icd10Api()
.icd10Code('A0')
.longDescription()
.search()
.then((icd10Info)=>{
    console.log(`${JSON.stringify(icd10Info)}`)
})
.catch((err)=>console.log(`ERROR, err=${err}`))

```
* output
```
{
  "Search": [
    {
      "Name": "A00",
      "Description": "Cholera"
    },
    {
      "Name": "A01",
      "Description": "Typhoid and paratyphoid fevers"
    },
    {
      "Name": "A02",
      "Description": "Other salmonella infections"
    },
    {
      "Name": "A03",
      "Description": "Shigellosis"
    },
    {
      "Name": "A04",
      "Description": "Other bacterial intestinal infections"
    },
    {
      "Name": "A05",
      "Description": "Other bacterial foodborne intoxications, not elsewhere classified"
    },
    {
      "Name": "A06",
      "Description": "Amebiasis"
    },
    {
      "Name": "A07",
      "Description": "Other protozoal intestinal diseases"
    },
    {
      "Name": "A08",
      "Description": "Viral and other specified intestinal infections"
    },
    {
      "Name": "A09",
      "Description": "Infectious gastroenteritis and colitis, unspecified"
    }
  ],
  "totalResults": "10",
  "Response": "True"
}
```

# resources #

* [icd10api.com](http://www.icd10api.com/)
* [icd10-api on bitbucket](https://bitbucket.org/javapda/icd10-api)
* [user agents](https://www.technipages.com/google-chrome-change-user-agent-string)
