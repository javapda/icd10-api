const Icd10Api = require('../icd10-api')

new Icd10Api()
.icd10Code('A0')
.longDescription()
.search()
.then((icd10Info)=>{
    console.log(`${icd10Info}`)
    console.log(`${JSON.stringify(icd10Info)}`)
})
.catch((err)=>console.log(`ERROR, err=${err}`))
