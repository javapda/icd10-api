const axios = require('axios')
module.exports = class Icd10Api {
    constructor() {
        this.config = {
            baseUrl : `http://www.icd10api.com/`,
            desc : `short`,
            code : null,
            s: null,
            type : null,
            r : 'json'
        }
    }
    icd10Code(code) {
        this.config.code = code
        this.config.s = code
        return this
    }
    longDescription() {
        this.config.desc = 'long'
        return this
    }
    shortDescription() {
        this.config.desc = 'short'
        return this
    }
    type(type) {
        this.config.type=(type)?type:null
        return this
    }
    page(pageNumber) {
        if(pageNumber) {
            this.config.page = Math.min(Math.max(0,pageNumber),100)
        }
    }
    async search() {
        console.log(`searching time ${this.config.baseUrl}`)
        if (!this.config.code) {
            throw "missing icd10Code, did you forget to call icd10Code(...)?"
        }
        return (await axios({
            method: 'get',
            url: `${this.config.baseUrl}`,
            params: this.config,
            headers:{'X-Source':'icd10-api'}
          })).data
    }

}